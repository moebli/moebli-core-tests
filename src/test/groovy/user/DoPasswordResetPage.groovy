package user

import geb.Page

class DoPasswordResetPage extends Page {

    static at = { waitFor(){
        $("#do-reset-password-form").isDisplayed()
    } }

    static content = {
        resetCode { $("#do-reset-password-form input[name=resetCode]") }
        phoneOrEmail { $("#do-reset-password-form input[name=phoneOrEmail]") }
        newPassword { $("#do-reset-password-form input[name=newPassword]") }
        resetButton { $("#do-reset-password-form .btn") }
        alertDanger { $("#do-reset-password-form .alert-danger") }
        alertSuccess { $("#do-reset-password-form .alert-success") }
        initPasswordResetLink { $('a[href="/start-password-reset"]')}
    }
}
