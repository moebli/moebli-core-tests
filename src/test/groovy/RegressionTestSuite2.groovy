import billsAndBalances.CreateBillItemNameSpec
import billsAndBalances.InitialBillsAndBalancesSpec
import billsAndBalances.CreateBillNewBalancesSpec
import billsAndBalances.CreateBillNewUserSpec
import billsAndBalances.CreateBillValidationSpec
import billsAndBalances.CreateBillExistingBalancesSpec
import billsAndBalances.UpdateBillToFromChangedSpec
import org.junit.runner.RunWith
import org.junit.runners.Suite
import payment.RecordPaymentSpec

@RunWith(Suite.class)
@Suite.SuiteClasses([InitialBillsAndBalancesSpec, CreateBillValidationSpec, CreateBillNewUserSpec,
        CreateBillNewBalancesSpec, UpdateBillToFromChangedSpec, CreateBillExistingBalancesSpec,
        CreateBillItemNameSpec, RecordPaymentSpec])
// CreateBillExistingBalancesSpec creates new contact for dev-01, so move to end
class RegressionTestSuite2 {
}
