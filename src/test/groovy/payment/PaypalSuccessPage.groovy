package payment

import geb.Page

class PaypalSuccessPage extends Page {

    static at = {
        waitFor(45) {
            $("#content-wrapper h3").text() == "Payment Completed"
        }
    }
}
