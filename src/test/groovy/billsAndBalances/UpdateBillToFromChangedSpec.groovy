package billsAndBalances

import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

// updateing Florence Frederick - 17mile drive
@Stepwise
class UpdateBillToFromChangedSpec extends GebReportingSpec {

    def updateBillSetup() {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage

        // Jan 2015
        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage

        // 17 mile drive expenses
        when:
        $('#balances-panel .balances > a', 2).click()
        then:
        at BillsPage

    }

    def updateBillFromContactBillsPage() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage

        // Florence Frederick
        when:
        $('#balances-panel .balances > a', 2).click()
        then:
        at BillsPage
        waitFor(5) {
            $('#bills-panel .bills .amount', text: '$0.55').isDisplayed()
        }

        // 17 mile drive
        when:
        $('#bills-panel .bills .amount', text: '$0.55').parent().find('.edit').click()
        then:
        at UpdateBillPage
        waitFor(10) {
            amount.value() == '0.55'
        }

        when:
        payToContact.click()
        billDate.value('01/03/2015')
        amount.value('.56')
        updateButton.click()
        then:
        at BillsPage
        waitFor(10) {
            at BillsPage
            $('#bills-panel .billDate', text: '01/03').text() == '01/03'
            $('#bills-panel h3 .balance span').text() == '$0.36'
        }
    }

    def updateBillFromMonthlyBillsPage() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage
        waitFor(10) {
            viewMonthlyBalances.isDisplayed()
        }

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .header', text: 'Jan 2015').parent().$('.balance span').text() == '$0.01'
        }

        // Jan 2015
        when:
        $('#balances-panel .balances .balance', text: '$0.01').click()
        then:
        at BillsPage

        // Frederick Florence -- 17 mile drive
        when:
        $('#bills-panel .bills .amount', text: '$0.56').parent().find('.edit').click()
        then:
        at UpdateBillPage
        waitFor(10) {
            amount.value() == '0.56'
        }

        when:
        billDate.value('02/03/2015')
        amount.value('.57')
        updateButton.click()
        then:
        at BillsPage
        waitFor(10) {
            at BillsPage
            $('#bills-panel .billDate', text: '02/03').text() == null
        }

        when:
        $('#bills-panel h3 a').click()
        then:
        at BalancesPage
        $('#balances-panel .balance span', text: '$0.57').isDisplayed()

        // Feb 2013
        when:
        $('#balances-panel .balance span', text: '$0.57').click()
        then:
        at BillsPage
        $('#bills-panel .bills .amount', text: '$0.57').isDisplayed()
    }

    def updateBillFromItemBillsPage() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .header', text: '17 mile drive expenses').parent().$('.balance span').text() == '$0.57'
        }

        when:
        $('#balances-panel .header', text: '17 mile drive expenses').click()
        then:
        at BillsPage

        when:
        $('#bills-panel .bills .amount', text: '$0.57').parent().find('.edit').click()
        then:
        at UpdateBillPage
        waitFor(10) {
            amount.value() == '0.57'
        }

        // resetting to initial value
        when:
        amount.value('.55')
        billDate.value('01/23/2015')
        payToMe.click()
        updateButton.click()
        then:
        at BillsPage
        waitFor(10) {
            at BillsPage
            $('#bills-panel h3 .balance span', text: '$0.55').isDisplayed()
        }
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage
        waitFor(5) {
            logoutLink.isDisplayed()
        }

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
