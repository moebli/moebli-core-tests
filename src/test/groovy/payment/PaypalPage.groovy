package payment

import geb.Page

class PaypalPage extends Page{

    static at = {
        waitFor(30) {
            $("#header h1").text() == "test facilitator's Test Store"
        }
    }

    static content = {
        loginLink { $("#loadLogin") }
        loginBox { $("#loginBox") }
        loginEmail { loginBox.$("#login_email") }
        loginPassword { loginBox.$("#login_password") }
        submitLoginButton { loginBox.$("#submitLogin") }
        payButton { $("#continueButtonSection").$("#step").$("#submit.x") }

    }
}
