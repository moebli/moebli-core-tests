package billsAndBalances

import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class InitialBillsAndBalancesSpec extends GebReportingSpec {

    def "contactBalances"() {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 5
            $("#balances-panel .balances > a", 0).$("span").text() == "\$0.55"
            $("#balances-panel .balances > a", 1).$("span").text() == "\$0.20"
            $("#balances-panel .balances > a", 2).$("span").text() == "\$0.75"
            $("#balances-panel .balances > a", 3).$("span").text() == ""
            $("#balances-panel .balances > a", 4).$("span").text() == ""
        }
    }

    def "contactBills"() {
        when:
        $('#balances-panel .balances > a', 1).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 2
        }
    }

    def "monthlyBalances"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1
        }
    }

    def "monthlyBills"() {
        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 5
        }
    }

    def "itemBalances"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 4
        }
    }

    // Eduardo Balsamo
    def "itemBills"() {
        when:
        $('#balances-panel .balances > a', 1).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 2
        }
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
