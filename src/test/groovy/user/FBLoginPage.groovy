package user

import geb.Page

class FBLoginPage extends Page {

    static at = {
        $("#facebook .login_page").isDisplayed()
    }

    static content = {
        email { $('#email') }
        password { $('#pass') }
        loginButton { $('#u_0_2') }
    }
}