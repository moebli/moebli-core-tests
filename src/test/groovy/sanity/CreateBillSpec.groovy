package sanity

import billsAndBalances.BalancesPage
import billsAndBalances.CreateBillConfPage
import billsAndBalances.CreateBillPage
import geb.spock.GebReportingSpec
import spock.lang.Stepwise

@Stepwise
class CreateBillSpec extends GebReportingSpec{

    def "go to create Bill page"(){
        given:
        at BalancesPage

        when:
        navBar.createBillLink.click()

        then:
        at CreateBillPage
    }

    def "create bill to pay to me"(){

        given:
        at CreateBillPage

        when:
        itemName = "Farewell Party"
        payTo = "me"
        contact0 = "dev-03@moebli.com"
        amount0="10"
        itemDate="04/25/2015"
        eventMessage="Farewell Party pay it"
        sendButton.click()

        then:
        at CreateBillConfPage

    }

    def "create bill pay to contact"() {
        given:
        at CreateBillConfPage

        when:
        to ViewBillPage
        navBar.createBillLink.click()

        then:
        at CreateBillPage

        when:
        itemName = "Welcome Party"
        payTo = "contact"
        contact0 = "dev-03@moebli.com"
        amount0="12"
        itemDate="04/25/2015"
        eventMessage="Welcome Party - need to pay you."
        sendButton.click()

        then:
        waitFor(10) {
            $("#create-bill-conf").$(".form-group").$("div.alert.alert-success").text() == "Welcome Party was sent"
        }
        //TODO Check Dynamic assertion of values for alert-success in Geb.
    }
}
