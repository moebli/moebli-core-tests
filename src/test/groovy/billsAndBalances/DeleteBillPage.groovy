package billsAndBalances

import geb.Page
import settings.NavBarModule

class DeleteBillPage extends Page {

    static at = {
        waitFor(5) {
            $("#bill-delete-modal .modal-title").text() == 'Delete Bill'
        }
    }

    static content = {
        deleteBillForm { $("#bill-delete-modal") }
        deleteButton { $("#delete-bill-button") }
    }
}
