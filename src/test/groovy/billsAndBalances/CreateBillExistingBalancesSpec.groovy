package billsAndBalances

import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class CreateBillExistingBalancesSpec extends GebReportingSpec {

    def createBillPage() {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 5
        }

        // Eduardo Balsamo
        when:
        $('#balances-panel .balances > a', 1).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 2
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1 || balancesCount() == 2
        }

        when:
        $('#balances-panel .balances div', text: 'Jan 2015').click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 5
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 4
        }

        // Cafe Prague
        when:
        $('#balances-panel .balances > a', 2).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }

        when:
        navBar.createBillLink.click()
        then:
        at CreateBillPage
    }

    def createBill() {
        given:
        at CreateBillPage
        when:
        itemName << "a"
        then:
        itemName.parent().find("ul").isDisplayed()
        itemName.parent().find("ul").find("li").size() == 2

        // Cafe Prague
        when:
        itemName.parent().find("ul").find("li").getAt(1).find("a").click()
        then:
        itemName.value().contains("Cafe")

        when:
        contact0 << "d"
        then:
        contact0.parent().find("ul").isDisplayed()
        contact0.parent().find("ul").find("li").size() == 5

        // Eduardo Balsamo
        when:
        contact0.parent().find("ul").find("li").getAt(1).find("a").click()
        amount0 = 15.00
        then:
        contact0.value().contains("Balsamo")

        // Christel Chung
        when:
        contact1 << "ch"
        then:
        contact1.parent().find("ul").isDisplayed()
        contact1.parent().find("ul").find("li").size() == 1

        when:
        contact1.parent().find("ul").find("li").getAt(0).find("a").click()
        amount1 = 10
        $("#more-line").click()
        then:
        contact1.value().contains("Chung")

        when:
        contact2 << "dev-07@moebli.com"
        amount2 = 12.0
        then:
        contact2.parent().find("ul").isDisplayed() == false

        when:
        contact3 { $("#create-bill-form input[name=contact3]") }
        contact3 << "dev-09@moebli.com"
        amount3 = 20.50
        then:
        contact3.parent().find("ul").isDisplayed() == false

        when:
        sendButton.click()
        then:
        at CreateBillConfPage
        successMessage.text().contains("Cafe")
        billRows.size() == 3
        blockedMessage.isDisplayed()
    }

    def checkNewContact() {
        when:
        createBillLink.click()
        then:
        at CreateBillPage

        when:
        contact0 << "Myriam"
        then:
        contact0.parent().find("ul").isDisplayed()
        contact0.parent().find("ul").find("li").size() == 1
    }

    def checkBalances() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 6
        }

        // Eduardo Balsamo
        // TODO if new contacted is sorted on client, index should be 1 and not 2
        when:
        $('#balances-panel .balances > a', 2).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 3
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1 || balancesCount() == 2
        }

        when:
        $('#balances-panel .balances div', text: 'Jan 2015').click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 8
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 4
        }

        // Cafe Prague
        when:
        $('#balances-panel .balances > a', 2).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 4
        }
    }

    // Myriam -- Caffe Prague
    def deleteBillFromItemBillsPage() {
        when:
        $('#bills-panel .bills .amount', text: '$20.50').parent().find('.delete').click()
        then:
        at DeleteBillPage
        waitFor(10) {
            deleteBillForm.$('.amount', text: '$20.50').isDisplayed()
        }

        when:
        deleteButton.click()
        then:
        at BillsPage
        waitFor(10) {
            ! billsContainer.$('.amount', text: '$20.50').isDisplayed()
        }
    }

    // Christel Chung
    def deleteBillFromContactBillsPage() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage

        when:
        $('#balances-panel .balances .balance span', text: '$10.55').click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 2
        }

        // Cafe Prague
        when:
        $('#bills-panel .bills .amount', text: '$10.00').parent().find('.delete').click()
        then:
        at DeleteBillPage
        waitFor(10) {
            deleteBillForm.$('.amount', text: '$10.00').isDisplayed()
        }

        when:
        deleteButton.click()
        then:
        at BillsPage
        waitFor(10) {
            ! billsContainer.$('.amount', text: '$10.00').isDisplayed()
        }
    }


    def deleteBillFromMonthlyBillsPage() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage
        waitFor(10) {
            viewMonthlyBalances.isDisplayed()
        }

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .header', text: 'Jan 2015').isDisplayed()
        }

        when:
        $('#balances-panel .header', text: 'Jan 2015').click()
        then:
        at BillsPage
        waitFor(10) {
            $('#bills-panel .bills .amount', text: '$15.00').isDisplayed()
        }

        // Eduardo Balsamo -- Cafe Prague
        when:
        $('#bills-panel .bills .amount', text: '$15.00').parent().find('.delete').click()
        then:
        at DeleteBillPage
        waitFor(10) {
            deleteBillForm.$('.amount', text: '$15.00').isDisplayed()
        }

        when:
        deleteButton.click()
        then:
        at BillsPage
        waitFor(10) {
            ! billsContainer.$('.amount', text: '$15.00').isDisplayed()
        }
    }

    def checkBalances2() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .balance span', text: '$0.20').isDisplayed()
            $('#balances-panel .balance span', text: '$0.55').isDisplayed()
            $('#balances-panel .balance span', text: '$0.75').isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .balance span', text: '$0.40').isDisplayed()
            $('#balances-panel .balance span', text: '$0.55').isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .balance span', text: '$1.10').isDisplayed()
        }
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
