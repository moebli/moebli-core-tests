package payment

import geb.Page

class MessagePage extends Page {

    static at = {
        waitFor(10) {
            $("#message-panel").isDisplayed()
        }
    }

    static content = {
        messagePanel { $("#message-panel") }
        pageTitle { messagePanel.$('.panel-title').text() }
        message { messagePanel.$('.panel-body').text() }
        continueButton { messagePanel.$(".btn") }
    }
}
