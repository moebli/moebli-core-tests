package settings

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class ChangeAccountNameSpec extends GebReportingSpec {

    def "changeAccountNamePage"() {
        when:
        to LoginPage
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changeAccountNameLink.click()
        then:
        at ChangeAccountNamePage
    }

    def "changeAccountNameError"() {
        when:
        accountName = "Seven/  <script> Eleven "
        changeButton.click()
        then:
        waitFor(2) {
            alertDanger.isDisplayed()
        }
    }

    def "changeAccountNameSuccess"() {
        when:
        accountName = "Seven-Eleven"
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }
    }

    def logout() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
