package payment

import settings.NavBarModule
import geb.Page

class PaymentWithPaypalPage extends Page{

    static at = {
        waitFor(5) {
            paymentForm.isDisplayed()
        }
    }

    static content = {
        navBar { module NavBarModule }
        paymentForm { $("#init-payment-form") }
        pageTitle { paymentForm.$('.title').text() }
        itemName { paymentForm.$("#itemName").$("input", name:"itemName") }
        amount { paymentForm.$("#amount").$("input", name:"amount") }
        externalNotes { paymentForm.$("#externalNotes").$("input", name:"externalNotes") }
        payWithPaypalButton { paymentForm.$(".paypal-button")}
    }
}
