package user

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import settings.ChangePasswordPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class PasswordResetSpec extends GebReportingSpec {

    def initReset() {
        when:
        to LoginPage
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        phoneOrEmail = "90"
        continueButton.click()
        then:
        waitFor() {
            alertDanger.isDisplayed()
        }

        when:
        phoneOrEmail = "dev-01@moebli.com"
        continueButton.click()
        then:
        waitFor() {
            alertSuccess.isDisplayed()
        }
    }

    // after 10 tries, too many retries message
    def doReset1() {
        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset2() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset3() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset4() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset5() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset6() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset7() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset8() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset9() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { alertDanger.isDisplayed() }
    }

    def doReset10() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "999000"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() {
            alertDanger.isDisplayed()
            alertDanger.text().contains("Invalid")
        }
    }

    // error message even with correct code because max retries exceeded
    def doReset11() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "123456"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() {
            alertDanger.isDisplayed()
            alertDanger.text().contains("Too many")
        }
    }

    def doResetSuccess() {
        when:
        initPasswordResetLink.click()
        then:
        at InitPasswordResetPage

        when:
        phoneOrEmail = "dev-01@moebli.com"
        continueButton.click()
        then:
        waitFor() {
            alertSuccess.isDisplayed()
        }

        when:
        doPasswordResetLink.click()
        then:
        at DoPasswordResetPage

        when:
        resetCode = "123456"
        phoneOrEmail = "dev-01@moebli.com"
        newPassword = "welcome2"
        resetButton.click()
        then:
        waitFor() { at BalancesPage }
    }

    // logout and then login with new password
    def reLogin() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage

        when:
        login("dev-01@moebli.com", "welcome2")
        then:
        waitFor() { at BalancesPage }
    }

    // change back password
    def changeBackPassword() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changePasswordLink.click()
        then:
        at ChangePasswordPage

        when:
        oldPassword = "welcome2"
        newPassword = "welcome1875"
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }
    }

    def logout() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
