package settings

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class ChangePasswordSpec extends GebReportingSpec {

    def "changePasswordPage"() {
        when:
        to LoginPage
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changePasswordLink.click()
        then:
        at ChangePasswordPage
    }

    def "changePasswordError"() {
        when:
        oldPassword = "welcome1874"
        newPassword = "welcome1874"
        changeButton.click()
        then:
        alertDanger.isDisplayed()
    }

    def "changePasswordSuccess"() {
        when:
        oldPassword = "welcome1875"
        newPassword = "welcome1874"
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

    def "loginWithNewPassword"() {
        when:
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1874"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changePasswordLink.click()
        then:
        at ChangePasswordPage
    }

    def "changePasswordSuccess2"() {
        when:
        oldPassword = "welcome1874"
        newPassword = "welcome1875"
        changeButton.click()
        then:
        at ChangePasswordPage
    }

    def logout() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
