package user

import geb.spock.GebReportingSpec
import settings.SettingsPage
import billsAndBalances.BalancesPage
import spock.lang.Stepwise

/* this test requires test user data cleanup */
@Stepwise
class FBLoginSpec extends GebReportingSpec {

    def "fbLinkAccountError"() {
        when:
        to LoginPage
        fbLoginButton.click()
        then:
        at FBLoginPage

        when:
        email = "dev-05@moebli.com"
        pass = "welcome1875"
        loginButton.click()
        then:
        at FBLinkAccountPage

        when:
        password = "wrong"
        submitButton.click()
        then:
        at FBLinkAccountPage
        alertDanger.text().contains("Invalid password")
    }

    def "fbLinkAccountSuccess"() {
        when:
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

    def "fbLogin"() {
        when:
        fbLoginButton.click()
        then:
        at BalancesPage
    }

    def "logout2"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
