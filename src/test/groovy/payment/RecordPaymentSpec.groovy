package payment

import billsAndBalances.BalancesPage
import billsAndBalances.BillsPage
import billsAndBalances.DeleteBillPage
import billsAndBalances.UpdateBillPage
import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class RecordPaymentSpec extends GebReportingSpec {

    def checkBalances() {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .balance span', text: '$0.20').isDisplayed()
            $('#balances-panel .balance span', text: '$0.55').isDisplayed()
            $('#balances-panel .balance span', text: '$0.75').isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .balance span', text: '$0.40').isDisplayed()
            $('#balances-panel .balance span', text: '$0.55').isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            $('#balances-panel .balance span', text: '$1.10').isDisplayed()
        }
    }

    def recordPayment() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage

        // Eduardo Balsamo
        when:
        $('#balances-panel .balances .balance span', text: '$0.20').click()
        then:
        at BillsPage

        when:
        $('#bills-panel a.payment').click()
        then:
        at RecordPaymentPage

        when:
        itemName << '1'
        then:
        then:
        itemName.parent().find("ul").isDisplayed()
        itemName.parent().find("ul").find("li").size() == 2

        // 17 mile drive expenses
        when:
        itemName.parent().find("ul").find("li").getAt(0).find("a").click()
        contactPaidMe.click()
        amount0 << '0.3'
        itemDate << '03/01/2015'
        externalNotes << 'Settled'
        then:
        itemName.value().contains("17 mile")

        when:
        sendButton.click()
        then:
        at BillsPage
        isAmountDisplayed('$0.30')
        isPaymentAmountDisplayed('$0.30')
    }

    def toUpdateAmount() {
        when:
        toUpdateAmount('$0.30')
        then:
        at UpdateBillPage

        when:
        iPaidContact.click()
        billDate.value('04/01/2015')
        updateButton.click()
        then:
        at BillsPage
        isAmountDisplayed('$0.30')
        isPaymentAmountDisplayed('$0.30')
    }

    def deletePayment() {
        when:
        toDeleteAmount('$0.30')
        then:
        at DeleteBillPage

        when:
        deleteButton.click()
        then:
        at BillsPage
        billsCount() == 0
    }

    def checkBalances2() {
        when:
        contactBalancesButton.click()
        then:
        at BalancesPage
        isBalanceDisplayed('$0.20')
        isBalanceDisplayed('$0.55')
        isBalanceDisplayed('$0.75')

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        isBalanceDisplayed('$0.40')
        isBalanceDisplayed('$0.55')

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        isBalanceDisplayed('$1.10')
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
