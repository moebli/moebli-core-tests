package sanity

import geb.spock.GebReportingSpec
import payment.PaymentWithPaypalPage
import payment.PaypalPage
import payment.PaypalReviewPage
import payment.PaypalSuccessPage

class PaymentWithPaypalSpec extends GebReportingSpec{

    def "go to recordPayment.sanity.RecordPaymentPage" () {

        given:
        at RecordPaymentConfPage

        when:
        seeAllButton.click()

        then:
        at ViewBillPage

        when:
        expandButton.click()
        expandButton.click()

        then:
        waitFor(30) {
            viewBillByConnection.$(".payment.btn.btn-link").text() == "Payment"
        }

        when:
        viewBillByConnection.$(".payment.btn.btn-link").click()

        then:
        at RecordPaymentPage
    }

    def "pay by paypal" () {

        given:
        at RecordPaymentPage

        when:
        itemName = "Bachelor Party"
        paymentMethod = "paypal"

        then:
        at PaymentWithPaypalPage

        when:
        itemName = "Bachelor Party"
        amount = "10"
        externalNotes = "paying through paypal"
        paywithPaypalButton.click()

        then:
        at PaypalPage

        when:
        loginLink.click()

        then:
        waitFor(30) {
            $("#loginModule").$("#method-paypal").$('.subhead').text() == "Log in to your PayPal account"
        }

        when:
        loginEmail = "dev-02@moebli.com"
        loginPassword = "welcome1875"
        submitLoginButton.click()

        then:
        at PaypalPage
        waitFor(30) {
            $("#funding-mix").$(".paymentType").$("#balance").text() == "\$10.00 USD"
        }

        when:
        at PaypalReviewPage
        payButton.click()

        then:
        at PaypalSuccessPage

    }
}
