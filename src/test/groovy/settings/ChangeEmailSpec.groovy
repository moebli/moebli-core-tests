package settings

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class ChangeEmailSpec extends GebReportingSpec {

    def "changeEmailPage"() {
        when:
        to LoginPage
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changeEmailLink.click()
        then:
        at ChangeEmailPage
    }

    def "changeEmailError"() {
        when:
        email = "notanemail"
        changeButton.click()
        then:
        waitFor() {
            alertDanger.isDisplayed()
        }
    }

    def "changeEmailSuccess"() {
        when:
        email = "dev-00@moebli.com"
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

    def "loginWithNewEmail"() {
        when:
        phoneOrEmail = "dev-00@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changeEmailLink.click()
        then:
        at ChangeEmailPage
    }

    def "changeEmailSuccess2"() {
        when:
        email = "dev-01@moebli.com"
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }
    }

    def logout() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
