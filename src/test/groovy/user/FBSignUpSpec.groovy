package user

import geb.Page
import settings.SettingsPage
import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import spock.lang.Stepwise

/* this test requires test user data cleanup */
/* also remove dev-11 authorization in FB */

@Stepwise
class FBSignUpSpec extends GebReportingSpec {

    def "fbOAuthCancel"() {
        when:
        to LoginPage
        fbLoginButton.click()
        then:
        at FBLoginPage

        when:
        email = "dev-11@moebli.com"
        pass = "welcome1875"
        loginButton.click()
        then:
        Page.at FBOAuthPage

        when:
        cancelButton.click()
        then:
        Page.at FBCancelPage
    }

    def "fbSignUp"() {
        when:
        continueButton.click()
        then:
        at LoginPage

        when:
        fbLoginButton.click()
        then:
        Page.at FBOAuthPage

        when:
        // editInfo.click()
        // email.click()
        // back.click()
        confirmButton.click()
        then:
        at BalancesPage
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
