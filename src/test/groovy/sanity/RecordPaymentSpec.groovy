package sanity

import geb.spock.GebReportingSpec
import payment.RecordPaymentConfPage
import payment.RecordPaymentPage


class RecordPaymentSpec extends GebReportingSpec{

    def "go to payment.sanity.RecordPaymentPage"() {
        given:
        at ViewBillPage

        when:
        expandButton.click()

        then:
        waitFor(30) {
            viewBillByConnection.$(".payment.btn.btn-link").text() == "Payment"
        }

        when:
        viewBillByConnection.$(".payment.btn.btn-link").click()

        then:
        at RecordPaymentPage
    }

    def "Amount null" () {
        given:
        at RecordPaymentPage

        when:
        itemName = "Bachelor Party"
        paymentMethod = "cash"
        xPaidy = "IPaidContact"
        itemDate ="04/25/2015"
        externalNotes = "Amount null check"
        sendButton.click()

        then:
        at RecordPaymentPage
        $("#payment-form").$(".form-group").$("div.alert.alert-danger").text() == "Please provide the required information"

    }

    def "I paid contact" () {

        given:
        at RecordPaymentPage

        when:
        itemName = "Bachelor Party"
        paymentMethod = "cash"
        xPaidy = "IPaidContact"
        amount0 = "10"
        externalNotes = "Paying to contact test"
        sendButton.click()

        then:
        at RecordPaymentConfPage

    }

}
