package payment

import geb.Page

class PaypalReviewPage extends Page {

    static at = {
        waitFor(30) {
            $("#header h1").text() == "test facilitator's Test Store"
        }
    }

    static content = {
        payButton { $("#continueButtonSection").$("#step").$(".button.primary.default.parentSubmit") }
    }
}
