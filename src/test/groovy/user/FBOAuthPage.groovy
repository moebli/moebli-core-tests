package user

import geb.Page

class FBOAuthPage extends Page {

    static at = {
        $("#facebook #platformDialogForm").isDisplayed()
    }

    static content = {
        editInfo { $("a:contains('Edit the info you provide')") }
        email { $("span:contains('Email address')") }
        back { $("span:contains('Back')") }
        cancelButton { $("button[name=__CANCEL__]") }
        confirmButton { $("button[name=__CONFIRM__]") }
    }
}