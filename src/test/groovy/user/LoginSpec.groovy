package user

import geb.spock.GebReportingSpec
import billsAndBalances.BalancesPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class LoginSpec extends GebReportingSpec {

    def "missingFields"() {
        when:
        to LoginPage
        then:
        at LoginPage

        when:
        submitButton.click()
        then:
        alertDanger.text().contains("Email is required")
        alertDanger.text().contains("Password is required")
    }

    def "invalidEmail"() {
        when:
        login("thisisnotemail", "welcome123")
        then:
        waitFor(2) {
            alertDanger.text() ==  "Invalid email or password"
        }
    }

    def "invalidPassword"() {
        when:
        phoneOrEmail = "dev-01@moebli.com"
        password = "abc"
        submitButton.click()
        then:
        at LoginPage
        alertDanger.text() ==  "Invalid email or password"
    }

    def "loginSuccess"() {
        when:
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
