package billsAndBalances

import geb.spock.GebReportingSpec
import settings.SettingsPage
import spock.lang.Stepwise
import user.LoginPage

@Stepwise
class CreateBillItemNameSpec extends GebReportingSpec {

    def createBillPage() {
        when:
        to LoginPage
        login("dev-03@moebli.com", "welcome1875")
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        $('#balances-panel .header', text: 'Russian river whitewater rafting').isDisplayed()
        balancesCount() == 3
    }

    def createBill() {
        when:
        navBar.createBillLink.click()
        then:
        at CreateBillPage

        when:
        itemName << "russian river   WhiteWaterRafting"
        contact0 << "dev-06@moebli.com"
        amount0 << "2.00"
        payToContact.click()
        sendButton.click()
        then:
        at CreateBillConfPage
        waitFor(10) {
            successMessage.text().contains("WhiteWaterRafting")
            billRows.size() == 1
            !blockedMessage.isDisplayed()
        }
    }

    def checkBalances() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        balancesCount() == 3
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
