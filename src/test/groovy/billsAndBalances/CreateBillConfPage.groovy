package billsAndBalances

import settings.NavBarModule
import geb.Page

// TODO replace with BillsPage
class CreateBillConfPage extends Page {

    static at = {
        waitFor(10) {
            $("#bills-panel").isDisplayed()
        }
    }

    static content = {
        navBar { module NavBarModule }
        createBillConf { $("#bills-panel") }
        successMessage { createBillConf.$(".conf-message") }
        billRows { createBillConf.$(".bills").children() }
        blockedMessage { createBillConf.$(".blocked-msg-div") }
        createBillLink { createBillConf.$(".create-another") }
    }
}
