package user

import geb.Page
import geb.spock.GebReportingSpec
import billsAndBalances.BalancesPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class SignUpSpec extends GebReportingSpec {

    def "signUpError"() {
        when:
        to LoginPage
        then:
        at LoginPage

        when:
        signUpLink.click()
        then:
        at SignUpPage

        when:
        firstName = "Dev"
        lastName = "12"
        email = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        waitFor(10) {
            alertDanger.isDisplayed()
        }
    }

    def "signUpSuccess"() {
        when:
        email = "dev-12@moebli.com"
        submitButton.click()

        then:
        at BalancesPage
    }

    def "loginAsNewUser"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage

        when:
        phoneOrEmail = "dev-12@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
