package settings

import geb.Page

class SettingsPage extends Page {

    static at = { waitFor(){
        $("#settings-menu a[href='#/change-email']").isDisplayed()
    } }

    static content = {
        changeEmailLink { $("#settings-menu a[href='#/change-email']") }
        changePasswordLink { $("#settings-menu a[href='#/change-password']") }
        changeNameLink { $("#settings-menu a[href='#/change-name']") }
        changeAccountNameLink { $("#settings-menu a[href='#/change-account-name']") }
        changeAddressLink { $("#settings-menu a[href='#/change-account-name']") }
        notificationSettingsLink { $("#settings-menu a[href='#/notification-settings']") }
        contactSettingsLink { $("#settings-menu a[href='#/friend-settings']") }
        viewMonthlyBalances { $("#settings-menu a[href='#/monthly-balances']") }
        viewItemBalances { $("#settings-menu a[href='#/event-balances']") }
        logoutLink { $("#settings-menu a[href='/logout']") }
    }
}
