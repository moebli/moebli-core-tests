package billsAndBalances

import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class CreateBillValidationSpec extends GebReportingSpec {

    def createBillPage() {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage

        when:
        navBar.createBillLink.click()
        then:
        at CreateBillPage
    }

    def missingFields() {
        given:
        at CreateBillPage
        when:
        sendButton.click()
        then:
        alertDanger.isDisplayed()
    }

    def invalidFields() {
        given:
        at CreateBillPage
        when:
        itemName = "Science City"
        contact0 = "invalid email"
        amount0 = "invalid amount"
        sendButton.click()
        then:
        alertDanger.isDisplayed()
    }

    def addRemoveLines() {
        when:
        at CreateBillPage
        then:
        $("#contact-group").children().size() == 3

        when:
        $("#less-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 2 }

        when:
        $("#less-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 1 }
        $("#less-line").isDisplayed() == false

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 2 }
        $("#less-line").isDisplayed()

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 3 }

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 4 }

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 5 }

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 6 }

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 7 }

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 8 }

        when:
        $("#more-line").click()
        then:
        waitFor(1) { $("#contact-group").children().size() == 9 }

        when:
        $("#more-line").click()
        then:
        $("#contact-group").children().size() == 10
        $("#more-line").isDisplayed() == false
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
