package sanity
import org.junit.runner.RunWith
import org.junit.runners.Suite
import payment.PaymentWithPaypalSpec

@RunWith(Suite.class)
@Suite.SuiteClasses([LoginSpec.class, CreateBillSpec.class, ViewBillSpec.class, RecordPaymentSpec.class,
        ])
class SanityTestSuite {
}
