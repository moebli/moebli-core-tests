package sanity

import geb.spock.GebReportingSpec
import spock.lang.Stepwise

/**
 * Created by sushant on 24/05/15.
 */
@Stepwise
class ViewBillSpec extends GebReportingSpec {

    def "check connections and connection balances"() {

        when:
        to ViewBillPage

        then:
        at ViewBillPage
        $("#view-bills-by-conn").$("div.balance").text() == "\$0.20"
    }
}
