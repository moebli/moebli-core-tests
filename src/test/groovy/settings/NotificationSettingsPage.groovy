package settings

import geb.Page

class NotificationSettingsPage extends Page {

    static at = { waitFor(){
        $("#notification-settings").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        billEmailUnsubscrib { $("#notification-settings-form input[name=billEmailUnsubscribe]") }
        changeButton { $("#notification-settings-form .btn") }
        alertSuccess { $("#notification-settings-form .alert-success") }
    }
}
