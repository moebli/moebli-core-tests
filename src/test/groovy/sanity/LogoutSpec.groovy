package sanity

import settings.SettingsPage
import geb.spock.GebReportingSpec
import user.LoginPage

class LogoutSpec extends GebReportingSpec {

    def "logout"() {

        when:
        to ViewBillPage
        navBar.settingsLink.click()

        then:
        at SettingsPage

        when:
        $("#settings").$("div#settings-menu.list-group").$("a[href='/logout']").click()

        then:
        at LoginPage
    }
}
