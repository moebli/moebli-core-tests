package settings

import geb.Page

class ChangeEmailPage extends Page {

    static at = { waitFor(){
        $("#change-email-form").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        email { $("#change-email-form input[name=email]") }
        changeButton { $("#change-email-form .btn") }
        alertDanger { $("#change-email-form .alert-danger") }
        alertSuccess { $("#change-email-form .alert-success") }
    }
}
