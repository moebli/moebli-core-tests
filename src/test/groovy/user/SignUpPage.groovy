package user

import geb.Page;

class SignUpPage extends Page {

    static url = "http://dev.payminder.com/signup"

    static at = { waitFor() {
        $("#sign-up-form").isDisplayed()
    } }

    static content = {
        signUpForm { $('#sign-up-form') }
        firstName { signUpForm.$('input[name=firstName]') }
        lastName { signUpForm.$('input[name=lastName]') }
        email { signUpForm.$('input[name=email]') }
        password { signUpForm.$('input[name=password]') }
        submitButton { signUpForm.$('button') }
        alertDanger { signUpForm.$(".alert-danger") }
    }
}