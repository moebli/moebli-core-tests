package billsAndBalances

import geb.Page
import settings.NavBarModule

class UpdateBillPage extends Page {

    static at = {
        waitFor(5) {
            $("#edit-bill-form").isDisplayed()
        }
    }

    static content = {
        editBillForm { $("#edit-bill-form") }
        itemName(wait: true) { editBillForm.$('#item-group').$('input', name:'itemName')}
        payTo { editBillForm.$('#pay-to-group').$('input', name:'payTo') }
        payToMe { editBillForm.$('input', name:'payTo', value:'me') }
        payToContact { editBillForm.$('input', name:'payTo', value:'contact') }
        amount { editBillForm.$('input', name:'amount') }
        billDate { editBillForm.$('input', name:'billDate') }
        eventMessage { editBillForm.$(".form-group").$('input', name:'externalNotes') }
        updateButton { $("#update-bill-button") }
        alertDanger { editBillForm.$(".alert-danger") }

        // for update recorded payment
        iPaidContact { editBillForm.$("#xPaidY").$('input', name:'xPaidY', value:'IPaidContact') }
        contactPaidMe { editBillForm.$("#xPaidY").$('input', name:'xPaidY', value:'ContactPaidMe') }
    }
}
