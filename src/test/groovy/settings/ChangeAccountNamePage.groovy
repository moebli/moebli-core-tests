package settings

import geb.Page

class ChangeAccountNamePage extends Page {

    static at = { waitFor(){
        $("#change-account-name-form").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        accountName { $("#change-account-name-form input[name=accountName]") }
        changeButton { $("#change-account-name-form .btn") }
        alertDanger { $("#change-account-name-form .alert-danger") }
        alertSuccess { $("#change-account-name-form .alert-success") }
    }
}
