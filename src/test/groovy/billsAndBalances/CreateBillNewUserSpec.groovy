package billsAndBalances

import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class CreateBillNewUserSpec extends GebReportingSpec {

    def emptyBalances() {
        when:
        to LoginPage
        login("dev-07@moebli.com", "welcome1875")
        then:
        at BalancesPage
        waitFor(10) {
            addButton.isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            addButton.isDisplayed()
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            addButton.isDisplayed()
        }
    }

    def createBill() {
        when:
        navBar.createBillLink.click()
        then:
        at CreateBillPage

        when:
        itemName << "Stinson Beach"
        contact0 << "dev-05@moebli.com"
        amount0 << "5.00"
        sendButton.click()
        then:
        at CreateBillConfPage
        successMessage.text().contains("Stinson")
        billRows.size() == 1
        ! blockedMessage.isDisplayed()
    }

    def checkBalances() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1
            !addButton.isDisplayed()
        }

        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1
            !addButton.isDisplayed()
        }

        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1
            !addButton.isDisplayed()
        }

        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
