package settings

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class ContactSettingsSpec extends GebReportingSpec {

    def contactSettingsPage() {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        contactSettingsLink.click()
        then:
        at ContactSettingsPage
        rows.size() == 6
        rowsDisplayed.size() == 5
        blockedRows.size() == 1
        invitedByRows.size() == 1
        connectedOrInvitedRows.size() == 4
    }

    def reconnect() {
        when:
        showBlockedCheckBox = true
        then:
        rowsDisplayed.size() == 6

        when:
        blockedRows.getAt(0).find("a").click()
        then:
        true

        when:
        blockedRows.getAt(0).find(".reconnect").find("a").click()
        then:
        true
    }

    // TODO decline invite; move this to suite 3
    /*
    def acceptInvite() {
        when:
        invitedByRows.getAt(0).find("a").click()
        then:
        true

        when:
        invitedByRows.getAt(0).find(".accept-invite").find("a").click()
        then:
        true
    }
    */

    def blockEmail() {
        when:
        connectedOrInvitedRows.getAt(0).find("a").click()
        then:
        true

        when:
        connectedOrInvitedRows.getAt(0).find(".block-email").find("a").click()
        then:
        true
    }

    def disconnect() {
        when:
        connectedOrInvitedRows.getAt(1).find("a").click()
        then:
        true

        when:
        connectedOrInvitedRows.getAt(1).find(".disconnect").find("a").click()
        then:
        true

        when:
        // need to re-initiate because of error "Element is no longer attached to the DOM"
        rows { $("#friend-settings .rows > div") }
        connectedOrInvitedRows { rows.find(".connected-or-invited") }
        connectedOrInvitedRows.getAt(1).find("a").click()
        then:
        true

        when:
        connectedOrInvitedRows.getAt(1).find(".disconnect").find("a").click()
        then:
        true
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

    def checkUpdates() {
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        contactSettingsLink.click()
        then:
        at ContactSettingsPage
        rows.size() == 6
        rowsDisplayed.size() == 4
        blockedRows.size() == 2
        invitedByRows.size() == 1
        /*
        try {
            invitedByRows
            false // exception should be raised
        } catch (RequiredPageContentNotPresent e) {
            true
        }
        */
        connectedOrInvitedRows.size() == 3

        when:
        connectedOrInvitedRows.getAt(0).find("a").click()
        then:
        connectedOrInvitedRows.getAt(0).find(".accept-email").size() == 1
    }

    // reset connected and disconnected so that next tests in suite can run
    def reset() {
        when:
        showBlockedCheckBox = true
        then:
        rowsDisplayed.size() == 6

        when:
        connectedOrInvitedRows.getAt(0).find("a").click()
        then:
        true

        when:
        connectedOrInvitedRows.getAt(0).find(".accept-email").find("a").click()
        then:
        true

        when:
        connectedOrInvitedRows.getAt(2).find("a").click()
        then:
        true

        when:
        connectedOrInvitedRows.getAt(2).find(".disconnect").find("a").click()
        then:
        true

        when:
        rows { $("#friend-settings .rows > div") }
        blockedRows { rows.find(".blocked") }
        blockedRows.getAt(0).find("a").click()
        then:
        true

        when:
        blockedRows.getAt(0).find(".reconnect").find("a").click()
        then:
        true

        when:
        rows { $("#friend-settings .rows > div") }
        blockedRows { rows.find(".blocked") }
        blockedRows.getAt(0).find("a").click()
        then:
        true

        when:
        blockedRows.getAt(0).find(".reconnect").find("a").click()
        then:
        rows { $("#friend-settings .rows > div") }
        rows.size() == 6
        rowsDisplayed.size() == 6
        rows.find(".blocked").size() == 1
        rows.find(".invited_by").size() == 1
        rows.find(".connected-or-invited").size() == 4
    }

    def "logout2"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
