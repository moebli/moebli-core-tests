import user.FBLoginSpec
import user.LoginSpec
import org.junit.runner.RunWith
import org.junit.runners.Suite
import settings.ChangeAccountNameSpec
import settings.ChangeEmailSpec
import settings.ChangeNameSpec
import settings.ChangePasswordSpec
import settings.ContactSettingsSpec
import settings.NotificationSettingsSpec
import user.SignUpSpec
import user.PasswordResetSpec

@RunWith(Suite.class)
@Suite.SuiteClasses([LoginSpec, FBLoginSpec, PasswordResetSpec, ChangeNameSpec, ChangeEmailSpec, ChangePasswordSpec,
ChangeAccountNameSpec, NotificationSettingsSpec, ContactSettingsSpec, SignUpSpec])
class RegressionTestSuite1 {
}
