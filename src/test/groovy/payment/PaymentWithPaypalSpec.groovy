package payment

import billsAndBalances.BalancesPage
import billsAndBalances.BillsPage
import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage

class PaymentWithPaypalSpec extends GebReportingSpec{

    def "payWithPayPal" () {
        when:
        to LoginPage
        login("dev-01@moebli.com", "welcome1875")
        then:
        at BalancesPage

        when:
        toContactBills('Eduardo Balsamo')
        then:
        at BillsPage
        balance() == '$0.20'

        when:
        paymentButton.click()
        then:
        at RecordPaymentPage

        when:
        payWithPayPal.click()
        then:
        at PaymentWithPaypalPage
        pageTitle.contains('Eduardo Balsamo')

        when:
        itemName << "Alu"
        then:
        itemName.parent().find("ul").isDisplayed()

        when:
        itemName.parent().find("ul").find("li").getAt(0).find("a").click()
        amount << '2'
        externalNotes << 'paying through paypal'
        payWithPaypalButton.click()
        then:
        at PaypalLoginPage
        waitFor(10) {
            loginLink.isDisplayed()
        }

        when:
        loginLink.click()
        then:
        waitFor(10) {
            $("#loginModule").$("#method-paypal").$('.subhead').text() == "Log in to your PayPal account"
        }

        when:
        loginEmail = "dev-01@moebli.com"
        loginPassword = "welcome1875"
        submitLoginButton.click()
        then:
        at PaypalLoginPage
        waitFor(10) {
            $("#funding-mix").$(".paymentType").$("#balance").text() == "\$2.00 USD"
        }

        when:
        at PaypalReviewPage
        payButton.click()
        then:
        at MessagePage
    }

    def backFromPayPal() {
        when:
        at MessagePage
        then:
        waitFor(20) {
            pageTitle.contains('Payment Completed')
            message.contains('$2.00')
            message.contains('Eduardo Balsamo')
            message.contains('Alumni Picnic')
        }

        when:
        continueButton.click()
        then:
        at BalancesPage
        isBalanceDisplayed('$1.80')

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
