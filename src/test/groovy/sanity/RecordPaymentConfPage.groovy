package sanity

import settings.NavBarModule
import geb.Page

class RecordPaymentConfPage extends Page {

    static at = {
        waitFor(45) {
            $("#record-payment-conf").$(".form-group").$("div.alert.alert-success").text() == "Payment for Bachelor Party was recorded"
        }
    }

    static content = {
        navigation { module NavBarModule }
        recordAnotherButton { $("#record-payment-conf").$(".form-group").$("#record-payment-button") }
        seeAllButton { $("#record-payment-conf").$(".form-group").$("a") }
    }
}

