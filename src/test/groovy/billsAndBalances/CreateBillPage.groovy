package billsAndBalances

import settings.NavBarModule
import geb.Page


class CreateBillPage extends Page {

    static at = {
        waitFor(5) {
            $("#create-bill-form").isDisplayed()
        }
    }

    static content = {
        navBar { module NavBarModule }
        createBillForm { $("#create-bill-form") }
        itemName(wait: true) { createBillForm.$('#item-group').$('input', name:'itemName')}
        payTo { createBillForm.$('#pay-to-group').$('input', name:'payTo') }
        payToContact { createBillForm.$('#pay-to-group').$('input', name:'payTo', value:'contact') }
        toConnGroup { createBillForm.$("#contact-group") }
        contact0 { toConnGroup.$("#line0").$('input', name:'contact0') }
        amount0 { toConnGroup.$("#line0").$('input', name:'amount0') }
        contact1 { toConnGroup.$("#line1").$('input', name:'contact1') }
        amount1 { toConnGroup.$("#line1").$('input', name:'amount1') }
        contact2 { toConnGroup.$("#line2").$('input', name:'contact2') }
        amount2 { toConnGroup.$("#line2").$('input', name:'amount2') }
        contact3 { toConnGroup.$("#line3").$('input', name:'contact3') }
        amount3 { toConnGroup.$("#line3").$('input', name:'amount3') }
        itemDate { createBillForm.$('input', name:'itemDate') }
        eventMessage { createBillForm.$(".form-group").$('input', name:'externalNotes') }
        sendButton { createBillForm.$("#create-bill-button") }
        alertDanger { createBillForm.$(".alert-danger") }
    }
}
