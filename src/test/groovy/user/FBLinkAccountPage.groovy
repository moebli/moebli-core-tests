package user

import geb.Page

class FBLinkAccountPage extends Page {

    static at = {
        $("#link-fb-user-form").isDisplayed()
    }

    static content = {
        form { $('#link-fb-user-form') }
        password { form.$('input[name=password]') }
        submitButton { form.$('button') }
        alertDanger { form.$(".alert-danger") }
    }
}