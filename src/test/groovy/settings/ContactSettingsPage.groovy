package settings

import geb.Page
import settings.NavBarModule

class ContactSettingsPage extends Page {

    static at = { waitFor() {
        $("#friend-settings").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        rows { $("#friend-settings .rows > div") }
        rowsDisplayed { rows.findAll { it.displayed } }
        blockedRows { rows.find(".blocked") }
        invitedByRows { rows.find(".invited_by") }
        connectedOrInvitedRows { rows.find(".connected-or-invited") }
        showBlockedCheckBox { $("#show-disconnected input") }
    }
}