package payment

import settings.NavBarModule
import geb.Page

// TODO delete
class RecordPaymentConfPage extends Page {

    static at = {
        waitFor(10) {
            $("#bills-panel").isDisplayed()
        }
    }

    static content = {
        navigation { module NavBarModule }
        recordPaymentConf { $("#bills-panel") }
        recordAnotherButton { recordPaymentConf.$(".record-another") }
        seeAllButton { recordPaymentConf.$(".contact-balances") }
    }
}

