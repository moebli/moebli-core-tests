package user

import geb.Page

class FBCancelPage extends Page {

    static at = { waitFor() {
        title == "Log In With Facebook Cancelled"
    } }

    static content = {
        continueButton { $("a.btn") }
    }
}