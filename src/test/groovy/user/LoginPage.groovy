package user

import geb.Page

class LoginPage extends Page {

    static url = "http://dev.payminder.com"

    static at = { waitFor() {
        $("#login-form").isDisplayed()
    } }

    static content = {
        loginForm { $('#login-form') }
        phoneOrEmail { loginForm.$('input[name=phoneOrEmail]') }
        password { loginForm.$('input[name=password]') }
        submitButton { loginForm.$('button') }
        alertDiv { loginForm.$(".alert") }
        alertDanger { loginForm.$(".alert-danger") }
        initPasswordResetLink { $('a[href="/start-password-reset"]')}
        signUpLink { $('a[href="/signup"]')}
        fbLoginButton { $('#fb-login-button') }
    }

    void login(String phoneOrEmail, String password) {
        this.phoneOrEmail = phoneOrEmail
        this.password = password
        submitButton.click()
    }
}
