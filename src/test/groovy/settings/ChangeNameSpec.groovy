package settings

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class ChangeNameSpec extends GebReportingSpec {

    def "changeNamePage"() {
        when:
        to LoginPage
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        changeNameLink.click()
        then:
        at ChangeNamePage
    }

    def "changeNameError"() {
        when:
        firstName = ""
        lastName = ""
        changeButton.click()
        then:
        alertDanger.isDisplayed()
    }

    def "changeNameError2"() {
        when:
        firstName = "!24"
        lastName = "32@"
        changeButton.click()
        then:
        alertDanger.isDisplayed()
    }

    def "changeNameSuccess"() {
        when:
        firstName = "Jennifer"
        lastName = "Aniston"
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }
    }

    def logout() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
