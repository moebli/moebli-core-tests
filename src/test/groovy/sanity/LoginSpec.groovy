package sanity

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class LoginSpec extends GebReportingSpec {

    def "invalidPassword"() {
        when:
        to LoginPage
        then:
        at LoginPage

        when: "Entered invalid password and submitted login form"
        phoneOrEmail = "dev-02@moebli.com"
        password = "abc"
        submitButton.click()
        then:
        at LoginPage
        $("#login-form").$(".form-group").$("div.alert.alert-danger").text() ==  "Invalid email or password"
    }

    def "should login with page valid username and password"() {
        when:
        to LoginPage
        then:
        at LoginPage

        when:"Entered userName, Password and submitted login Form"
        phoneOrEmail = "dev-02@moebli.com"
        password = "welcome1875"
        submitButton.click()

        then:"User Home Page should be shown with connection list"
        at BalancesPage

    }
}
