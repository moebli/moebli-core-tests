package settings

import geb.Module

class NavBarModule extends Module {
    static  content = {
        viewBillsLink { $("#view-bills-menu .navbar-link") }
        createBillLink { $("#bill-form-menu .navbar-link") }
        settingsLink { $("#view-settings-menu .navbar-link") }
    }
}
