package billsAndBalances

import settings.NavBarModule
import geb.Page

class BalancesPage extends Page {

    static at = { waitFor(10) {
        $('#balances-panel').isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        balancesContainer { $('#balances-panel .balances') }
        addButton { $('#balances-panel .add') }
    }

    int balancesCount() {
        return $('#balances-panel .balances > a').size();
    }

    boolean isBalanceDisplayed(balance) {
        waitFor(2) {
            balancesContainer.$('span', text: balance).isDisplayed()
        }
    }

    void toContactBills(contact) {
        balancesContainer.$('.header', text: contact).click()
    }
}