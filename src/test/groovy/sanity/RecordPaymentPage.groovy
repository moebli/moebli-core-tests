package sanity

import settings.NavBarModule
import geb.Page

class RecordPaymentPage extends Page{

    static url = "/bill/#/payment/87618fbf-25a0-4ad6-bc03-3479da4441f0"

    static at = {
        waitFor(30) {
            $("#payment-form").$(".form-group h3").text() == "Payment"
        }
    }

    static content = {
        navigation { module NavBarModule }
        paymentForm { $("#payment-form") }
        paymentMethod { paymentForm.$("#paymentMethod").$("input", name:"paymentMethod") }
        itemName(wait: true) { paymentForm.$('#itemName').$('input', name:'itemName')}
        xPaidy { paymentForm.$("#xPaidY").$("input", name:"xPaidY") }
        amount0 { paymentForm.$("#amount0").$("input", name:"amount0") }
        itemDate { paymentForm.$("#itemDate").$("input", name:"itemDate") }
        externalNotes { paymentForm.$("#externalNotes").$("input", name:"externalNotes") }
        sendButton { paymentForm.$(".form-group").$(".btn.btn-primary.btn-lg") }
    }
}
