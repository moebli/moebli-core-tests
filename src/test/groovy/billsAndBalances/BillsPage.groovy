package billsAndBalances

import geb.Page
import settings.NavBarModule

class BillsPage extends Page {

    static at = { waitFor(5){
        $("#bills-panel").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        billsContainer { $('#bills-panel .bills') }

        contactBalancesButton { $('#bills-panel .contact-balances') }
        paymentButton { $('#bills-panel .payment') }
    }

    int billsCount() {
        return $('#bills-panel .bills').children().size();
    }

    String balance() {
        return $('#bills-panel .balance span').text()
    }

    boolean isAmountDisplayed(amount) {
        waitFor(5) {
            return billsContainer.$('.amount', text: amount).isDisplayed()
        }
    }

    boolean isPaymentAmountDisplayed(amount) {
        waitFor(5) {
            return billsContainer.$('.amount', text: amount).parent().$('.billMessage code').text() == 'payment'
        }
    }

    void toUpdateAmount(amount) {
        isAmountDisplayed(amount)
        billsContainer.$('.amount', text: amount).parent().find('.edit').click()
    }

    void toDeleteAmount(amount) {
        isAmountDisplayed(amount)
        billsContainer.$('.amount', text: amount).parent().find('.delete').click()
    }
}