package billsAndBalances

import geb.spock.GebReportingSpec
import user.LoginPage
import settings.SettingsPage
import spock.lang.Stepwise

@Stepwise
class CreateBillNewBalancesSpec extends GebReportingSpec {

    def createBillPage() {
        when:
        to LoginPage
        login("dev-04@moebli.com", "welcome1875")
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 1
        }
    }

    def createBill() {
        when:
        navBar.createBillLink.click()
        then:
        at CreateBillPage

        when:
        itemName << "Moss Beach Kayak"
        contact0 << "dev-06@moebli.com"
        amount0 << "5.00"
        payToContact.click()
        sendButton.click()
        then:
        at CreateBillConfPage
        waitFor(10) {
            successMessage.text().contains("Moss Beach Kayak")
            billRows.size() == 1
            !blockedMessage.isDisplayed()
        }
    }

    def checkBalances() {
        when:
        navBar.viewBillsLink.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 3
        }

        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewMonthlyBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        viewItemBalances.click()
        then:
        at BalancesPage
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        $('#balances-panel .balances > a', 0).click()
        then:
        at BillsPage
        waitFor(10) {
            billsCount() == 1
        }
    }

    def "logout"() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }

}
