import org.junit.runner.RunWith
import org.junit.runners.Suite
import payment.PaymentWithPaypalSpec
import user.FBSignUpSpec

@RunWith(Suite.class)
@Suite.SuiteClasses([PaymentWithPaypalSpec, FBSignUpSpec])
class RegressionTestSuite3 {
}
