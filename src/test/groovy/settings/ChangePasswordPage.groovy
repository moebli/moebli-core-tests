package settings

import geb.Page

class ChangePasswordPage extends Page {

    static at = { waitFor(){
        $("#change-password-form").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        newPassword { $("#change-password-form input[name=newPassword]") }
        oldPassword { $("#change-password-form input[name=oldPassword]") }
        changeButton { $("#change-password-form .btn") }
        alertDanger { $("#change-password-form .alert-danger") }
        alertSuccess { $("#change-password-form .alert-success") }
    }
}
