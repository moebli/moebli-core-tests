package user

import geb.Page
import settings.NavBarModule

class InitPasswordResetPage extends Page {

    static at = { waitFor(){
        $("#init-reset-password-form").isDisplayed()
    } }

    static content = {
        phoneOrEmail { $("#init-reset-password-form input[name=phoneOrEmail]") }
        continueButton { $("#init-reset-password-form .btn") }
        alertDanger { $("#init-reset-password-form .alert-danger") }
        alertSuccess { $("#init-reset-password-form .alert-success") }
        doPasswordResetLink { $('a[href="/do-password-reset"]')}
    }
}
