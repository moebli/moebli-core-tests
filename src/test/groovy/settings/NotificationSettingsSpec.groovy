package settings

import billsAndBalances.BalancesPage
import geb.spock.GebReportingSpec
import user.LoginPage
import spock.lang.Stepwise

@Stepwise
class NotificationSettingsSpec extends GebReportingSpec {

    def "notificationSettingsPage"() {
        when:
        to LoginPage
        phoneOrEmail = "dev-01@moebli.com"
        password = "welcome1875"
        submitButton.click()
        then:
        at BalancesPage

        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        notificationSettingsLink.click()
        then:
        at NotificationSettingsPage
    }

    def "set"() {
        when:
        billEmailUnsubscribe = true
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }
    }

    def checkSet() {
        when:
        driver.navigate().back()
        then:
        at SettingsPage

        when:
        notificationSettingsLink.click()
        then:
        at NotificationSettingsPage
        waitFor(2) {
            billEmailUnsubscribe == "on"
        }
    }

    def "unset"() {
        when:
        billEmailUnsubscribe = false
        changeButton.click()
        then:
        waitFor(2) {
            alertSuccess.isDisplayed()
        }
    }

    def checkUnset() {
        when:
        driver.navigate().back()
        then:
        at SettingsPage

        when:
        notificationSettingsLink.click()
        then:
        at NotificationSettingsPage
        waitFor(2) {
            billEmailUnsubscribe == false
        }
    }

    def logout() {
        when:
        navBar.settingsLink.click()
        then:
        at SettingsPage

        when:
        logoutLink.click()
        then:
        at LoginPage
    }
}
