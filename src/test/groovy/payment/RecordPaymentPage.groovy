package payment

import settings.NavBarModule
import geb.Page

class RecordPaymentPage extends Page{

    static at = {
        waitFor(10) {
            $("#payment-form").isDisplayed()
        }
    }

    static content = {
        navigation { module NavBarModule }
        paymentForm { $("#payment-form") }
        payWithPayPal { paymentForm.$("#paymentMethod").$("input", name:"paymentMethod", value:'paypal') }
        itemName(wait: true) { paymentForm.$('#itemName').$('input', name:'itemName')}
        xPaidy { paymentForm.$("#xPaidY").$("input", name:"xPaidY") }
        iPaidContact { paymentForm.$("#xPaidY").$('input', name:'xPaidY', value:'IPaidContact') }
        contactPaidMe { paymentForm.$("#xPaidY").$('input', name:'xPaidY', value:'ContactPaidMe') }
        amount0 { paymentForm.$("#amount0").$("input", name:"amount0") }
        itemDate { paymentForm.$("#itemDate").$("input", name:"itemDate") }
        externalNotes { paymentForm.$("#externalNotes").$("input", name:"externalNotes") }
        sendButton { paymentForm.$(".form-group").$(".btn") }
    }
}
