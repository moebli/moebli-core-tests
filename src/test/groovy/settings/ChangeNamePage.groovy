package settings

import geb.Page

class ChangeNamePage extends Page {

    static at = { waitFor(){
        $("#change-person-name-form").isDisplayed()
    } }

    static content = {
        navBar { module NavBarModule }
        firstName { $("#change-person-name-form input[name=firstName]") }
        lastName { $("#change-person-name-form input[name=lastName]") }
        changeButton { $("#change-person-name-form .btn") }
        alertDanger { $("#change-person-name-form .alert-danger") }
        alertSuccess { $("#change-person-name-form .alert-success") }
    }
}
